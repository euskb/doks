# Reporting Security Issues

To report a security issue, drop an email to **aagern /AT/ gmail.com** and include the word "SECURITY" in the subject line.
