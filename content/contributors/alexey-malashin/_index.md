---
title: "Alexey Malashin"
description: "General Resources Manager of the EUS KB Portal"
date: 2022-01-12T00:00:00+03:00
lastmod: 2022-01-12T00:00:00+03:00
draft: false
images: []
---

Automation and CI/CD pipeline of the Portal.

[@mynameisnoname](https://t.me/mynameisnoname)
