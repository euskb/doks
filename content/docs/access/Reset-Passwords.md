---
title: "Reset Passwords"
description: "ROOT and Web Console Passwords"
lead: "Resetting procedures"
date: 2022-03-13T16:30:31+03:00
lastmod: 2022-03-13T16:30:31+03:00
draft: false
images: []
menu:
  docs:
    parent: "wone-access"
weight: 1000
toc: true
---

External Links:
-   Resetting passwords KB - [https://kb.vmware.com/s/article/2149950](https://kb.vmware.com/s/article/2149950);
-   PhotonOS on Github - [https://github.com/vmware/photon](https://github.com/vmware/photon);
-   API Call to [set password](https://github.com/vmware/idm/wiki/SCIM-guide#create-a-local-user-in-the-default-system-directory)

❗️The **admin** user password must be at least 12 characters in length.

## SSH User Password Change
Connect to WS1 Access Appliance by SSH, run command:
```bash

passwd sshuser

```

## Web Console Admin Password Reset
1. Log in to the URL as a root user:
[https://ApplianceFQDN:8443/cfg/changePassword](https://ApplianceFQDN:8443/cfg/changePassword)

2. Access WS1 Access using ssh (sshuser password needed), do **su** and use this command to reset password:

-   Admin console site:
```bash

/usr/sbin/hznAdminTool setOperatorPassword --pass newsecretpassword

```

-   Configurator page:
```bash

/usr/sbin/hznAdminTool setSystemAdminPassword --pass newsecretpassword

```

## Root User Password Reset
WS1 Access works on VMware PhotonOS variation of Linux. It has a Single User Mode.

1.  Go to vCenter Server list of VMs;
2. Right-click the affected WS1 Access OVA and click Open Console;
3. Under the VM menu, click **Power->Shut Down Guest**;
4. When the shutdown completes, Power On the WS1 Access Appliance;
6.  When the GNU GRUB menu displays, press **p** and enter the configured bootloader password = ⭐️ **H0rizon!** ⭐️ ;
     ❗️GRUB menu appears for a few seconds. If you miss it, reboot and try again;
7.  Use the up and down arrow keys to navigate to the first entry, and press **e** to edit the relevant boot parameters; 
8.  Use the arrow keys to navigate to the line beginning with kernel and press **e** to edit;
9.  The cursor is at the end of the line, type a **space** and then append ```init=/bin/bash``` to the line;
10.  Press Enter to confirm the changes;
11.  Press **b** to execute the boot;
12. After boot, you have ROOT access and you are able to set new passwords, for root: ``` passwd ``` and for sshuser: ``` passwd sshuser ```;
      ❗️Follow password policies on new passwords!
13. Shutdown by using command: ``` shutdown -h -P now ```;
      ❗️VMware Tools do NOT work in Single User Mode, so shutdown from vCenter will NOT work;
14. Start WS1 Access Appliance.
