---
title: "Sniffing Packets"
description: "How to sniff packets in WS1 Access"
lead: "Custom sniffer substituting tcpdump"
date: 2022-03-13T16:30:31+03:00
lastmod: 2022-03-13T16:30:31+03:00
draft: false
images: []
menu:
  docs:
    parent: "wone-access"
weight: 1001
toc: false
---

There is no tcpdump on WS1 Access, and there is no possibility to install it due to dependency problems (written & tested on **SUSE Ent Linux** version of WS1 Access).

Use Python2 to write your own sniffer of packets for Access-vIDM (fresh [build on GitHub](https://github.com/aagern/tools)):

```python

# Filename = sniffer.py
# Packet sniffer script 0.3
# Made by Alexei Rybalko for vIDM-Access Server
# Based on SUSE Ent. Linux 11 with python2

# Usage:
# python sniffer.py 192.168.1.1
# Will sniff any packets going from or coming into IP=192.168.1.1, includes ping-ICMP/TCP/UDP

import socket, sys
from struct import *
 
if __name__ == "__main__":
    s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(0x0003))

    if (not sys.argv[1]):
        print("Enter IP Address to filter packets from!")
        sys.exit(0)
 
    while True:
        packet = s.recvfrom(65565)[0]
        eth_header = packet[:14]
        eth = unpack('!6s6sH', eth_header)
        eth_protocol = socket.ntohs(eth[2])
 
        if eth_protocol == 8: # IP
            ip_header = packet[14:34]
            iph = unpack('!BBHHHBBH4s4s', ip_header)
            ttl = iph[5]
            protocol = iph[6]
            s_addr = socket.inet_ntoa(iph[8])
            d_addr = socket.inet_ntoa(iph[9])
            #print "Source IP: " + s_addr
            #print "Destination IP: " + d_addr
 
            if (s_addr == sys.argv[1]) or (d_addr == sys.argv[1]): # IP Address only the one provided as argument to script
                if protocol == 6: # TCP
                    tcp_header = packet[20:40]
                    tcph = unpack('!HHLLBBHHH', tcp_header)
                    source_port = tcph[0]
                    dest_port = tcph[1]
                    print("--TCP--")
                    print "Source port: " + str(source_port)
                    print "Destination port: " + str(dest_port)
 
                elif protocol == 1: # ICMP
                    icmp_header = packet[20:24]
                    icmph = unpack('!BBH', icmp_header)
                    icmp_type = icmph[0]
                    code = icmph[1]
                    checksum = icmph[2]
                    print("--ICMP--")
                    print "Type: " + str(icmp_type)
                    print "Code: " + str(code)
 
                elif protocol == 17: # UDP
                    udp_header = packet[20:28]
                    udph = unpack('!HHHH', udp_header)
                    source_port = udph[0]
                    dest_port = udph[1]
                    print("--UDP--")
                    print "Source port: " + str(source_port)
                    print "Destination port: " + str(dest_port)
 
                else:
                    print('Unknown Protocol!')

```