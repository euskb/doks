---
title: "SSL offloading on UAG"
description: "SSL offloading on UAG"
lead: "Notes about SSL offloading for different servicer in UEM"
date: 2022-02-19T16:30:31+03:00
lastmod: 2022-02-19T16:30:31+03:00
draft: false
images: []
menu:
  docs:
    parent: "wone-uem"
weight: 1000
toc: false
---

##SSL offloading on UAG

SSL offload to external LoadBalancer usually a good practice to improve performance DS's and UAG servers, avoiding it to extra work with encrypt traffic
Also you should to change public Certificate in one place instead of replace it in each server

> **Note:** all above are valid if you have alot devices registred in WS1 (more than 2000). For smaler deployments usually you may stay at much easy deployment without load balancers.

In case of SSL offloadin you should use one of the scenarios:

1) Traffic isn't encrypted after termination on LoadBalancer  
![SSL termination without reencryption](SSL-offload-on-UAG-001.png) 

This can be used for traffic to Devcie Services endpoint only.
Even if you setup `SSL offload` check box during DS installation AWCM endpoint will use encriprion with self-generated certificate for traffic on port `2001`. In this case you need to enshure tant LoadBalancer trust for this cert (or ignores SSL errors)

<span style="color:red">❗️All you **internal** connection to AWCM must go though LoadBalancer enpoint to proper trust</span>

1) Traffic is reencrypted after termination on LoadBalancer with local cert  
![SSL termination without reencryption](SSL-offload-on-UAG-002.png) 

Despite the availability `SSL offload` check box in SEG and Content setup in WS1 Console, UAG server can work only with encripted http-traffic.

[SEG](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/2111/WS1-Secure-Email-Gateway/GUID-9FBF1F5D-D1B8-4DE3-8281-8D8E6F4A8A1F.html)  
The UAG does not support any non-encrypted protocols. Therefore, SEG only supports SSL re-encryption (SSL bridging) or SSL pass through  

[Content:](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/services/Content_Gateway/GUID-AWT-T-MIGRATING-CG.html)  
HTTP traffic is not allowed for Content Gateway on port 80 on Unified Access Gateway because TCP port 80 is used by the edge Service Manager.