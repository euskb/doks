---
title: "WS1 UEM .NET Globalization Settings"
description: "IIS Globalization settings for DS"
lead: "Solving issue with localized Windows servers"
date: 2022-02-18T16:30:31+03:00
lastmod: 2022-02-18T16:30:31+03:00
draft: false
images: []
menu:
  docs:
    parent: "wone-uem"
weight: 1001
toc: false
---

## WS1 UEM .NET Globalization settings

**Issue**

Usually, on-prem installation of WS1 UEM is doing with localized Windows servers (French, Russian, as an examples). With that, default settings in IIS can be set up incorrectly. This issue may prevent enrolling Windows devices (iOS or Android devices enroll well at the same time).

**Solution**

To fix this isseu you need to change .NET Globalization settings for Default Website level for all DS servers in your environment:

Parameter	|Value
---			|---
**Culture**	|
Culture	|English (United States) (en-US)
UI Culture|English (United States) (en-US)
**Encoding**	|
File		|Windows-1252

![.NET-Globalization-Settings](Globalization-001.png ".NET Globalization Settings")
