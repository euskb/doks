---
title: "WS1 UEM Content installation Certificate error"
description: "WS1 UEM Content installation error wiht public certificate"
lead: "Some workaround for RSA error in WS1 Content Setup"
date: 2022-02-19T16:31:31+03:00
lastmod: 2022-02-18T19:31:31+03:00
draft: false
images: []
menu:
  docs:
    parent: "wone-uem"
weight: 1000
toc: false
---

## WS1 UEM Content installation Certificate error

In some rare cases (for UAG release 2106 or maybe later) installation Content Gateway on UAG can be failed with messager in logs like this:

~~~
13:10:03.127 [main]  INFO c.v.enterprise.content.Application - Starting Application v21.06.0 on <UAG-hostname> with PID 18264 (/opt/vmware/content-gateway/content-gateway.jar started by gateway in /opt/vmware/content-gateway)  
13:10:03.132 [main]  INFO c.v.enterprise.content.Application - No active profile set, falling back to default profiles: default. 
13:10:05.159 [main]  INFO  c.v.enterprise.content.Application - PostConstruct: Application instance created.
13:10:05.850 [main]  ERROR c.v.e.c.security.AuthenticatorImpl - **Exception while generating public key from modulus and exponent**
13:10:05.851 [main]  WARN  o.s.c.a.AnnotationConfigApplicationContext - Exception encountered during context initialization - cancelling refresh attempt: org.springframework.beans.factory.UnsatisfiedDependencyException: Error creating bean with name 'server': Unsatisfied dependency expressed through field 'connectivityAdapter'; nested exception is org.springframework.beans.factory.UnsatisfiedDependencyException: Error creating bean with name 'connectivity': Unsatisfied dependency expressed through field 'authenticator'; nested exception is org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'authenticatorImpl': Invocation of init method failed; nested exception is java.lang.IllegalStateException: java.security.spec.InvalidKeySpecException: java.security.InvalidKeyException:  
**RSA keys must be at least 512 bits long**
13:10:05.877 [main]  ERROR o.s.boot.SpringApplication - Application startup failed
~~~

❗️This artice is only workaround for error and don't contain deep explanation root cause problem analyze

1. If you already upload public certificate to **Groups & Settings** > **All Settings** > **System** > **Enterprise Integration** > **Content Gateway**, delete it and save settings
2. Option step: upload/update your default UAG certificate for UAG console in **TLS Server Certificate Settings** pointed it to Internet Interface (more detailed here: [Update SSL Server Signed Certificates](https://docs.vmware.com/en/Unified-Access-Gateway/2106/uag-deploy-config/GUID-59A33B87-B3E9-4A80-9348-1AE841EDF567.html))
3. Enable Content service in **Edge Service Settings**. It should be started after a couple of minutes with green light. At this stage you may notice URL your content gateway is reacheble, but get TLS/SSL error
4. Repopulate your public certificete deleted on Stage 1.
5. Disable and Enable Content service in **Edge Service Settings** to reread settings from API server. After that your Content Gateway URL should work as expected with your public certificate.