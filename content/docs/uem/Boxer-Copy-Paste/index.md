---
title: "Boxer Copy&Paste restriction in two ways"
description: "Boxer Copy&Paste restriction"
lead: "Two way in configure Boxer restrictions"
date: 2022-02-18T16:30:31+03:00
lastmod: 2022-02-18T16:30:31+03:00
draft: false
images: []
menu:
  docs:
    parent: "wone-uem"
weight: 1000
toc: false
---

##Boxer Copy&Paste restrictions in two ways

Unlike all other WS1 SDK-enabled Apps, Boxer has two different approaches to restrict Copy&Paste:

---
At the Assignment stage in **App Policies**, there is a Copy Paste setting. As a result, copy&paste functions will be denied in **ANY** directions

```
❗️Unrestricted personal mail accounts in Boxer still can be troublmakers in this case. Recommend to disable it
```

![Copy&Paste Total restrict](boxer-copy-paste-001.png)

---
If you need to do more granular restriction you need to implement this on SDK profile

- Recommended to set Native Boxer DLP capability to **Unrestricted**. It can be **Restricted** for potential more secure way, but SDK settings must be enabled **after** this settings
- The Boxer App must be published with SDK-profile enable. We use the Default profile, but it should work with Custom SDK-profile as well
- Before actual install Apps on devices, you need setup SDK-profile: Authentication Type **=/=** Disable; SSO **must be enabled**
- In DLP section (Security Policies) you may enable Copy&Paste Into to get user possibility to copy from unmanaged messengers/notes/etc into Boxer emails

As a result, copy&paste functions will be denied only in the desired way: into or out from managed Apps
![Copy&Paste Total unrestrict](boxer-copy-paste-002.png)
![Enable SDK](boxer-copy-paste-003.jpg)
![SSO enable](boxer-copy-paste-004.jpg)
![Copy&Paste In and Out](boxer-copy-paste-005.jpg)